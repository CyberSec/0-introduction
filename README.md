# Introduction à la cybersécurité des applications

## Introduction
Cette présentation est réalisée notamment à partir de celle de [Valentin Brosseau][introduction_securite].

## Intégration continue
Le [diaporama] est généré avec pandoc, puis hébergé sur les pages Framagit.

## Licence
Tous les fichiers sont sous licence MIT.

[introduction_securite]: https://cours.brosseau.ovh/cours/introduction_securite.html
[diaporama]: https://cybersec.frama.io/0-introduction
