% Sécurité des applications WEB
% par [Moulinux](https://moulinux.frama.io/) et [Valentin Brosseau](https://cours.brosseau.ovh/)
% (révision 01.11.2023)

---

### Pour vous, la sécurité c’est quoi ?

---

## Des découvertes de failles tous les jours !

---

### Des exemples en tête ?

---

![Faille 1](./img/faille1.png)

---

![Faille 2](./img/faille2.png)

---

![Faille 3](./img/faille3.png)

---


### Comment se tenir à jour ?

Prévenir plutôt que guérir…

---

### Des sites à surveiller

- [CERT-FR](https://www.cert.ssi.gouv.fr/cti/) / [US CERT](https://www.us-cert.gov/)
- [The Hacker News](http://thehackernews.com/)
- [Zataz](https://www.zataz.com/)
- [Undernews](https://www.undernews.fr/)
- [IT Connect](https://www.it-connect.fr/actualites/actu-securite/)
- [Reddit NetSec](https://www.reddit.com/r/netsec/)
- Next INpact (~Payant)

---

#### La sécurité informatique dans une application c’est un « équilibre »

---

### Le prix de la sécurité

- Impact fonctionnel
- Limitation de l’experience utilisateur (UX)
- Impact financier

---

### Les facteurs d’expositions

![Facteur d'exposition](./img/expositions.png)

---

### L’insécurité a également un coût

## 3,75 millions d’euros en France

- Estimation du coût moyen d’un piratage selon le baromètre annuel d'IBM
(*Cost Of Data Breach*) : [Voir l'article de ZDNet](https://www.zdnet.fr/actualites/selon-ibm-le-cout-moyen-d-un-piratage-s-etablit-a-375-millions-d-euros-en-france-39960848.htm)
- Les cyberattaques ont coûté deux milliards d'euros aux organisations
françaises en 2022 : [Voir l'article d'usine digitale](https://www.usine-digitale.fr/article/les-cyberattaques-ont-coute-deux-milliards-d-euros-aux-organisations-francaises-en-2022.N2144612)
- Plus de 40% de cette somme est destinée au paiement de rançons.

---

### Mais, une faille c’est quoi ?

![Une faille ?](./img/faille.png)

---

### Les type de failles

- L’humain (Social Engineering)
- Applicatif (Hack)
- L’argent (À partir de combien une personne donne l’information ?)

---

### Et si je ne fais rien !?

- Volontaire !
- Involontaire ?

---

### Intégrer la sécurité à toutes les étapes

La sécurité, c’est un état d’esprit à intégrer.

C’est le métier des :

- Admin Réseau / Système.
- **Développeur**.

---

### Améliorer la sécurité

- Dès la conception
- Comprendre et avoir en tête les règles

---

### Open Web Application Security Project (OWASP)

#### Les [dix risques](https://owasp.org/Top10/) par ordre de dangerosité (maj sept. 2021)

---

![OWASP TOP 10](./img/owasp_top-10.png)

---

### 1. Broken Access Control

- Les contrôles d'accès appliquent une politique assurant que les utilisateurs respectent leurs permissions.
- Une faille entraînera généralement des fuites d'informations, des corruptions ou destructions de données, ou permettra des actions en dehors des autorisations de l'utilisateur.

---

### 1. Broken Access Control ([Scénario #1](https://github.com/OWASP/Top10/blob/master/2017/fr/0xa5-broken-access-control.md))

L'application utilise des données non vérifiées dans un appel SQL qui accède aux informations d'un compte :

```raw
pstmt.setString(1, request.getParameter("acct"));
ResultSet results = pstmt.executeQuery();
```

En modifiant simplement le paramètre `acct` dans le navigateur, un attaquant peut envoyer le numéro de compte qu'il veut. Si ce numéro n'est pas vérifié, l'attaquant peut accéder à n'importe quel compte utilisateur.

```raw
http://example.com/app/accountInfo?acct=notmyacct
```

---

### 1. Broken Access Control ([Scénario #2](https://github.com/OWASP/Top10/blob/master/2017/fr/0xa5-broken-access-control.md))

Un attaquant force le navigateur à visiter des URL arbitraires. Il faut imposer des droits pour accéder à une page d'administration.

```raw
http://example.com/app/getappInfo
http://example.com/app/admin_getappInfo
```

Si un utilisateur non-authentifié peut accéder à l'une des pages, c'est une faille. Si un non-administrateur peut accéder à une page d'administration, c'est une faille.

---

### 2. Cryptographic Failures (ex. Sensitive Data Exposure)

Correspond aux failles de sécurité exposant des données sensibles comme les mots de passe, les numéros de carte de paiement ou encore les données personnelles et la nécessité de chiffrer ces données.

---

### 2. Cryptographic Failures (ex. Sensitive Data Exposure)

- Espace client sans SSL
- Mot de passe en clair (ou en MD5) dans la base de données

---

### 3. Injection

- Correspond au risque d’injection de commande (Système, SQL, Shellcode, etc.).
- 33 [CWE] (Common Weakness Enumeration)sont mappés dans cette catégorie.
- On y retrouve notamment les vulnérabilités XSS.

---

### 3. Injection > SQL

```sql
SELECT * FROM client WHERE id='" . $_GET["id"] . "'
```

```raw
http://exemple.com/liste?id='or '1'='1
```

---

### 3. Injection > Cross-Site Scripting XSS

Correspond à la non validation d'une entrée saisie par l'utilisateur. Permet notamment le vol de session.

---

### 3. Injection > Cross-Site Scripting XSS

Exécution de code Javascript sans validation

```html
Votre Nom : <input type="text" name="nom" value="" />
```

```js
alert("Bonjour " + $_POST["nom"]);
```

---

### 4. Insecure Design

Correspond à des faiblesses de conception dans l'implémentation de la solution.
Cette faiblesse est souvent caractérisée par des défauts de contrôle.

---

### 4. Insecure Design

- Récupération d'identifiants par des "questions et réponses", qui ne peut
pas être considérée comme des preuves d'identité car plus d'une personne peut connaître les réponses.
- Système de réservation de groupes, qui ne demande qu'un maximum de quinze participants avant d'exiger un dépôt. Les attaquants pourraient modéliser ce flux et tester s'ils peuvent réserver six cents places et tous les cinémas à la fois en quelques requêtes, ce qui entraînerait une perte de revenus massive.

---

### 5. Security Misconfiguration

Correspond aux failles liées à une mauvaise configuration des serveurs Web, applications, base de données ou framework.

---

### 5. Security Misconfiguration

- Console d’administration disponible sans authentification en ligne
- Listage des répertoires ([Exemple](https://www.startpage.com/search?dcr=0&q=intitle%3A%22Index%20of%22))
- Exemples de code non supprimés

---

### 6. Vulnerable and Outdated Components

Correspond aux failles liées à l’utilisation de composants tiers vulnérables.

---

### 6. Vulnerable and Outdated Components

- CMS non à jour.
- Apache / Tomcat non patchés.
- Librairies XYZ non à jour.

---

### 7. Identification and Authentication Failures

Correspond au risque de casser ou de contourner la gestion de l’authentification et de la session. Comprend notamment le vol de session ou la récupération de mots de passe.

---

### 7. Identification and Authentication Failures

```raw
http://exemple.com/?jsessionid=A2938298D293
```

---

### 8. Software and Data Integrity Failures

Correspond à des mises à jour de logiciels, données critiques, chaînes d'approvisionnement sans vérification de l'intégrité des sources.

---

### 8. Software and Data Integrity Failures

Par exemple, une désérialisation non sécurisée, qui conduit à l’exécution d'un code à distance.

---

### 9. Security Logging and Monitoring Failures

Une journalisation et une surveillance insuffisantes, couplées à une réponse inefficace aux incidents, permettent aux attaquants d’attaquer davantage les systèmes.

---

### 10. Server-Side Request Forgery

> Cette nouvelle entrée dans le TOP10 a été introduite dans un objectif de
sensibilisation, car bien qu'elle soit remontée par l'enquête auprès des
professionnels, elle n'a pas été mesurée comme ayant des impacts supérieurs
à la moyenne.

---

### 10. Server-Side Request Forgery

- La faille SSRF se produit lorsqu'une application web récupère une ressource distante sans valider l'URL fournie par l'utilisateur.
- Elle permet à un attaquant de contraindre l'application à envoyer une requête élaborée à une destination inattendue, même lorsqu'elle est protégée par un pare-feu, un VPN ou un autre type d'ACL réseau.

---

### 10. Server-Side Request Forgery

Par exemple, permettre l'accès à des fichiers sensibles comme `/etc/passwd`

---


### Les outils autour d’OWASP

- **Mutillidae** (Formation, PHP)
- OWASP Juice Shop (Formation, Javascript)
- WebGoat (Formation, Java)
- Zed Attack Proxy (Audit)
- OWASP Testing Guide (Guide pour voir le niveau de sécurité)
- OWASP Code Review Guide (Méthode d’audit)

---

#### Avec OWASP on parle de

### Vulnérabilité, et non de risque

---

### Sécurité à tous les niveaux

---

![Niveaux](./img/securite.png)

---

### Sécurité du réseau local

- IPS (Intrusion Prevension System)
- IDS (Intrusion Detection System)

---

### IPS

- Ils bloquent tout ce qui parait infectieux à leurs yeux, mais n'étant pas fiable à 100 % (risque de bloquer du trafic légitime).
- Ils laissent parfois passer certaines attaques sans les repérer.
- Peu discrets. Lors d'une attaque, l'attaquant s'empressera de trouver une faille dans ce dernier pour le contourner et arriver à son but.

---

### IDS

Un système de détection d'intrusion (Intrusion Detection System) est un mécanisme destiné à repérer des activités anormales ou suspectes sur la cible analysée

![IDS](./img/nids.png)

(Sans bloquer)

---

### Comment réduire le risque ?

---

- Sensibilisations / Formations.
- Revue de code (GIT).
- Tests d’intrusions (audit).
- Sécurité lors du recrutement.
- Sécurité dès la conception du projet.
- Suivi des bonnes pratiques (ex: OWASP).

---

### Pourquoi il faut se battre en entreprise ?

- Résistance au changement.
- ERP / CMS.
- La sécurité est vue comme un frein, pas une fonctionnalité.
- Déni de la réalité.
- Développement externe (prestations, stagiaires, …)

---

### Se former

#### En continu

- Conférence physique ou en ligne.
- Internet
- Appliquer les bonnes pratiques tout le temps !

---

### Se proteger (authenficiation)

- 2FA (par exemple avec **KeePassXC**)
- La big tech veut rendre les mots de passe obsolètes en imposant ses
solutions avec les risques que cela comporte
(comme [passkeys](https://securite.developpez.com/actu/349400/Google-veut-rendre-les-mots-de-passe-obsoletes-en-faisant-des-passkeys-l-option-par-defaut/) de Google) 

---

### Clé USB d'authentification

![Yubikey](./img/yubikey.png)

---

### Se proteger

Un dispositif comme la [YubiKey](https://fr.wikipedia.org/wiki/YubiKey) ou [Librem Key](https://puri.sm/products/librem-key/)
permet aux utilisateurs de s'authentifier de façon sécurisée à leurs comptes :

- en émettant un mot de passe à usage unique
- ou en utilisant une paire de clé privée/publique générée par le dispositif.

---

### Des questions ?

[CWE]: https://fr.wikipedia.org/wiki/Common_Weakness_Enumeration
